<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="es">

	<head>
        <title><?php echo isset($title) ? $title : 'Hugui Dugui'; ?></title>
        <meta charset="utf-8">
		<meta name="description" content="<?php echo isset($descripcion_seo) ? $descripcion_seo : ''?>">
		<meta name="keywords" content="<?php echo isset($tags_seo) ? $tags_seo : ''?>">
		<meta name="author" content="Hugui Dugui">
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
		<link rel="icon" href="<?php echo base_url();?>assets/img/hugui.ico" type="image/icon">

		<link href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo base_url();?>assets/vendor/icofont/icofont.min.css" rel="stylesheet">
		<link href="<?php echo base_url();?>assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
		<link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom.css?ver=<?php echo time();?>">
		
	</head>

	<body>
		