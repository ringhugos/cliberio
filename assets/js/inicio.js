$(document).ready(function(){
 	var server = "http://localhost:3000/";

    $.ajax({
        url: server + 'insurances',
        method:"GET",
        dataType: 'json',
        success:function(resp) {
             console.log(resp);
            // console.log("------");
            $("#seguros").empty();
            $("#seguros").html( sliderSeguros(resp) );
			
			//Carrusel
			$(".owl-carousel").owlCarousel({
		  		margin: 10,
		  		autoWidth: true,
		  		autoplay: true,
		  		autoplayTimeout: 1500
		 	});
        }
    });


    $.ajax({
        url: server + 'loans',
        method:"GET",
        dataType: 'json',
        success:function(resp) {
             console.log(resp);
            // console.log("------");
   //          $("#seguros").empty();
   //          $("#seguros").html( sliderSeguros(resp) );
			
			// //Carrusel
			// $(".owl-carousel").owlCarousel({
		 //  		margin: 10,
		 //  		autoWidth: true
		 // 	});
        }
    });
});

function sliderSeguros( data ) {

	var output = "<div  class='owl-carousel'>";

	//Crear los items de mapfre
	$.each( data.mapfre, function( key, value ) {
        output += "<div>" +
				  	  "<div class='p-3 card'>" +  
					  			"<div class='row'>" +
					  		"<div class='col-lg-12 titulo'>" +
					  			value.title +
					  		"</div>" +
					  		"<div class='col-lg-8 desde'>" +
					  			"Desde" +
					  		"</div>" +
					  		"<div class='col-lg-8 precio'>" +
					  			value.price_from + ' €/mes' +
					  		"</div>" +
					  	"</div>" +
				  	 "</div>" +
				  "</div>";
    });

	//Crear los items de linea-directa
    $.each( data["linea-directa"], function( key, value ) {
        output += "<div>" +
				  	  "<div class='p-3 card'>" +  
					  			"<div class='row'>" +
					  		"<div class='col-lg-12 titulo'>" +
					  			value.title +
					  		"</div>" +
					  		"<div class='col-lg-8 desde'>" +
					  			"Desde" +
					  		"</div>" +
					  		"<div class='col-lg-8 precio'>" +
					  			value.price_from + ' €/mes' +
					  		"</div>" +
					  	"</div>" +
				  	 "</div>" +
				  "</div>";
    });

	//Crear los items de mutua
    $.each( data.mutua, function( key, value ) {
        output += "<div>" +
				  	  "<div class='p-3 card'>" +  
					  			"<div class='row'>" +
					  		"<div class='col-lg-12 titulo'>" +
					  			value.title +
					  		"</div>" +
					  		"<div class='col-lg-8 desde'>" +
					  			"Desde" +
					  		"</div>" +
					  		"<div class='col-lg-8 precio'>" +
					  			value.price_from + ' €/mes' +
					  		"</div>" +
					  	"</div>" +
				  	 "</div>" +
				  "</div>";
    });

    output += '</div>';

	return output;
	
}